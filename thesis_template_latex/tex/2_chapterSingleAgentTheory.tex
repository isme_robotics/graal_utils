\chapter[Single Agent Control: Theory]{Single Agent Control:\\Theory}\label{ch:theory}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=\columnwidth]{theory/kcl_dcl_scheme}
		\caption{The overall architecture: the Kinematic Control Layer is the one implementing the proposed task priority approach, executing the current action scheduled by a Mission Manager; the output of the Kinematic Control Layer are the system velocities, tracked by the underlying Dynamic Control Layer.}\label{fig:control_scheme}
	\end{center}
\end{figure}

As anticipated in the introduction the control framework presented in this thesis is based on the cascade of blocks shown in Fig. \ref{fig:control_scheme}. In particular, the architecture is constituted by three main general blocks:
\begin{enumerate}
	\item The Mission Manager is in charge of supervising the execution of the current \emph{mission}, and generates the corresponding \emph{actions} to be executed by the Kinematic Control Layer. As it will be explained later in section \ref{sec:action}, an action is any prioritized list of control objectives to be concurrently achieved, and a mission is a sequence (or graph) of actions.
	\item The Kinematic Control Layer (KCL) implements the proposed task priority control framework, and is in charge of reactively accomplishing the \emph{control objectives} that make up the current action to be executed, generating the desired system velocity vector.
	\item The Dynamic Control Layer (DCL) tracks the desired system velocity vector by generating appropriate force/torques references for the vehicle and the manipulator.
\end{enumerate}

The thesis analysis focuses on the Kinematic Control Layer, since it is the one implementing the proposed task priority approach. The interfaces with the higher level (Mission Manager) and the lower one (DCL) are highlighted whenever relevant. By the definition we have given for the Mission Manager follows that it will be application dependent. In general it is the higher level layer in a given software architecture. Therefore we may also refer to it as the HLL (High Level Layer).

The unifying KCL control architecture presented can be applied both in the case of partial (assisted) teleoperation and fully autonomous operation. The main features of this architecture can be summarized as:
\begin{enumerate}[label={$\bm{F_{\arabic*}}$}]%
	\item inequality control objectives are handled without over-constraining the system, allowing them to be placed at higher priority whenever necessary, e.g. safety control objectives can have high priority (section \ref{sec:activation});
	\item switching between actions (an action is a set of prioritized control tasks) can be done easily, and allows the higher planning levels to interface with the control one in terms of such building blocks, which cover a great deal of underlying control details (section \ref{sec:actiontran}).
	\item the presence of vehicle underactuations can be managed, to the best extent possible, with the addition of a suitable highest priority non-reactive control task (section \ref{sec:underact});
	\item vehicle inaccuracies in tracking the desired velocity are not propagated to the end-effector thanks to a vehicle-arm coordination scheme, based on two parallel optimizations (section \ref{sec:vehiclearmcoord});
	\item the vehicle-arm coordination scheme naturally allows a multi rate control of the two subsystems (section \ref{sec:multirate});  
\end{enumerate}

\section{General Definitions}\label{sec:definitions}

Before starting the discussion we first introduce some notation in the next subsection. Then some definitions are given in the successive subsection, while some preliminaries on the regularized pseudo inverses are given in the last one.
\subsection{Notation}
Vectors and matrices are expressed with a bold face character, such as $\mat{M}$, whereas scalar values are represented with a normal font such as $\gamma$.
Given a matrix $\mat{M}$:
\begin{itemize}
	\item $M_{(i,j)}$ indicates the element of $\mat{M}$ at the $i$-th row and $j$-th column;
	%\item $\mat{M}_{[k]}$ refers to the $k$-th column of $\mat{M}$;
	%\item $\mat{M}_{k}$ or $m_k$ identifies the variable as belonging to a particular set $k$;
	\item $\mat{M}^\#$ is the exact generalized pseudo-inverse, i.e. the pseudo inverse of $\mat{M}$ performed without any regularizations.
\end{itemize}
Further, less used notation is introduced as needed.

\section{Definitions}
Let us introduce some definitions that will be used thoroughly in this paper. Here we will consider the general case of a floating dual-arm manipulator, which encompasses all the type of robots, then every other system considered successively will lye in a subspace derived by an appropriate downgrade of this general case. Let us define:
\begin{itemize}
	\item The system configuration vector $\vec{c} \in \mathbb{R}^n$ of a generic mobile manipulator as%
%
	\begin{equation}
	\vec{c} \triangleq  \begin{bmatrix}
						\vec{\eta} \\
						\vec{q}_L \\
						\vec{q}_R
						\end{bmatrix},
	\end{equation}%
	%
	where $\vec{\eta} \in \mathbb{R}^m$ is the vehicle \emph{generalized coordinate position vector} and $\vec{q}_L \in \mathbb{R}^{l}$ and $\vec{q}_R \in \mathbb{R}^{r}$ are the left and right arm configuration vectors, supposing that that the left arm has $l$ DOF and the right one has $r$ DOF. In particular $\vec{\eta}$ is the stacked vector of the position vector $\vec{\eta}_1$, with components on the inertial frame $\langle w \rangle$, and the orientation vector $\vec{\eta}_2$, the latter expressed in terms of the three angles yaw, pitch and roll (applied in this sequence) \cite{Perez2007}. From the above definitions it results $n = l + r + m$;
	\item The system velocity vector $\dot{\vec{y}} \in \mathbb{R}^n$ of the mobile manipulator as%
%
	\begin{equation}
	\dot{\vec{y}} \triangleq \begin{bmatrix}
							 \vec{v} \\
							 \dot{\vec{q}}_L \\
							 \dot{\vec{q}}_R
							 \end{bmatrix},
	\end{equation}%
	%
	 where $\vec{v} \in \mathbb{R}^{m}$ is the stacked vector of the vehicle linear velocity vector $\vec{v}_1$ and the vehicle angular velocity vector $\vec{v}_2$, both with components on the vehicle frame $\langle v \rangle$. While  $\dot{\vec{q}}_L \in \mathbb{R}^{l}$ and $\dot{\vec{q}}_R \in \mathbb{R}^{r}$ are the left and right arm joint velocities. For the time being, this work will be developed with the assumption of a vehicle fully actuated in its 6 degrees of freedom, therefore $m=6$, and the system velocity vector will coincide with the control vector. Section \ref{sec:underact} will show how to relax this hypothesis.
\end{itemize}

%\clearpage
\section{Control Objectives}\label{sec:objectives}
Let us start the discussion by considering what the robotic system needs to achieve. The goals of the system might include, for example, maintaining a certain distance from another vehicle, reaching a waypoint, or avoiding the arm joint limits. Mathematically speaking, let us consider a scalar variable $x(\vec{c})$, i.e. a variable dependent on the current configuration of the robot and the environment. Then, for this variable, two broad classes of objectives can be defined:%
%
\begin{enumerate}
	\item The requirement, for $t \to \infty$, that $x(\vec{c}) = x_0$ is called a \emph{scalar equality control objective}.
	\item The requirement, for $t \to \infty$, that $x(\vec{c}) < x_M$ or $x(\vec{c}) > x_m$ is called a \emph{scalar inequality control objective}.
\end{enumerate}

The mathematical symbols \bsq{$=$} and \bsq{$\lessgtr$} will be used throughout the thesis to identify equality and inequality scalar objectives respectively.\\
The use of scalar objectives is to simplify the presentation of the approach. However, let us remark that it is not a limitation of the approach. In fact, consider a vector $\vec{h} \in \mathbb{R}^m$, and consider $m$ different scalar objectives, one for each of its components. Then, all together the $m$ objectives allow to define a requirement for the whole vector $\vec{h}$, for example having some of its components within some ranges, while the others attaining certain values.

Other than the specific class, the control objectives can be divided in different categories, depending on their purpose and, as it will be clear later, on their priority. In fact, control objectives can be classified as:
\begin{itemize}
	\item[{\framebox[2.3\width]{\constrTask}}] physical \textbf{constraints} objectives, i.e. interacting with the environment;
	\item[{\framebox[2.3\width]{\safetyTask}}] system \textbf{safety} objectives, e.g. avoiding joint limits or obstacles;
	\item[{\framebox[2.3\width]{\prereqTask}}] objectives that are a \textbf{prerequisite} for accomplishing the mission, e.g. maintaining the manipulated object in the camera angle of view;
	\item[{\framebox[2.3\width]{\actdefTask}}] \textbf{action} defining objectives, i.e. what the system really needs to execute to accomplish the current user defined action;
	\item[{\framebox[2.3\width]{\optimTask}}] \textbf{optimization} objectives, i.e. objectives that do not influence the mission, but allow to choose between multiple solutions, if they exists.
\end{itemize}

The capitalized letters associated with the class types will be used throughout the thesis to identify a specific type of objective (the \nameref{nomenclature} section at the beginning can be used as a reference).

\subsection{Reactive Control Tasks}
To each scalar control objectives there is always associated a \emph{feedback reference rate} $\dot{\bar{x}}$, whose role is to drive the associated variable $x(\vec{c})$ toward an arbitrary point $x^*$, located inside the interval where the objective is satisfied. For instance, an example of feedback reference rate is the following one:
\begin{equation}\label{eq:fbkrefrate}
\dot{\bar{x}}(x) \triangleq \gamma (x^* - x),\,\gamma > 0,
\end{equation}
where $\gamma$ is a positive gain proportional to the desired convergence rate for the considered variable.

For each of the considered variables $x(\vec{c})$, the Jacobian relationship
\begin{equation}\label{eq:scalarjacrel}
\dot{x} = \vec{J} \dot{\vec{y}}
\end{equation}
links the rate of change of the variable itself with the system velocity vector $\dot{\vec{y}}$ through the vector $\vec{J} \in \mathbb{R}^n$. So, having the actual $\dot{x}$ as much as possible equal to the desired $\dot{\bar{x}}$ is termed a \emph{reactive control task}. The letter \bsq{\reactTask{}} will identify reactive tasks throughout the thesis.

\subsection{Non-reactive Control Tasks}
In the previous sections, the triple control objective, reactive control task and activation function has been defined. However, there are cases where a control task is defined directly in a certain task velocity space, without having an associated control objective. As an example, consider the case where a human operator wants to control the end-effector by generating velocity references through a joystick. In such a case, there is no control objective associated with the control task, because there is not a position to reach. In fact, the reference rate is generated by the user, rather than being the output of a feedback control loop as in \eqref{eq:fbkrefrate}.
The letters pair \bsq{\nonReactTask{}} will identify non-reactive tasks throughout the thesis.

\section{Actions and Missions}\label{sec:action}
In the terminology of this work, an \emph{action} is a prioritized list of control objectives and associated reactive tasks, with the possible addition of any non-reactive tasks, to be \emph{concurrently} managed. For example, for a robotic manipulator, a manipulation action could be represented by following list of objectives
\begin{tasklist}
	\item \JointLimitsTask
	\item \ManipTask
	\item \ToolFramePosTask
	\item \ToolFrameOriTask
	\item \ArmShapeTask
\end{tasklist}%
%
where it is natural to see \emph{safety} objectives (\safetyTask) such as joint limits at the highest priority. It is followed by the manipulability objective that allows for a better execution of the overall position objective, since it avoids the problems of high joint velocities occurring near kinematic singularities. Finally, an optimization objective such as the preferred shape can be defined, however its priority will be lower than the position control, to avoid over-constraining the system to a specific configuration. See Section \ref{sec:objectives} for more insight on task types. A more in-depth explanation for the single tasks will be introduced in the application section, every time a new control objective is introduced.

Further examples will be given in Chapter \ref{ch:applications}. For the time being, the following aspects can be highlighted:
\begin{itemize}
	\item In principle, within an action, there could be an arbitrary number of prioritized objectives, organized in different ways. However, following the categories of objectives presented in section \ref{sec:objectives}, usually the main difference between actions will be the objective(s) needed to achieve its particular ultimate goal and possibly some other prerequisite ones, while the safety tasks will be the same.
	\item Actions can be temporally sequenced accordingly with a \emph{mission} plan represented by a decisional action graph, having the actions as nodes and the arcs as logic decision alternatives, with just a few parameters relevant to the action ultimate-goal only.
	\item The aforementioned two points allow to highlight how the interface of the KCL with the Mission Manager should therefore result simplified, allowing an easier implementation of the latter.
	\item Moreover, transitions from an action to another one located at the end of a selected arc, should be smoothly activated.
\end{itemize}


\section{Activation Functions}\label{sec:activation}
For every control objective and relative reactive control task, there is always an associated activation function to be considered. The role of the activation function, as the name implies, is to specify whenever the control objective is relevant for the system. For example:
\begin{itemize}
	\item Maintaining the object to be grasped within the camera system cone of vision is relevant only when the object is close to the boundaries, since there is no need to have it \emph{exactly} centred; 
	\item Maintaining a joint away from its limit is relevant only in the vicinity of the limit itself, but it \emph{should not overconstrain} the system when the joint is sufficiently far away.
\end{itemize}

Motivated by the above considerations, let us define an activation function so composed:
\begin{equation}\label{eq:actfun}
a(x) = a^i(x),
\end{equation}
where $a^i(x) \in [0, 1]$ is continuous sigmoidal function of the objective variable $x$, which assumes zero values within the validity region of the associated objective.
For example, assuming an objective of the class $x(c) > x_{min}$:
\begin{equation}
	a^{i}(x)=
	\begin{cases}
	1,     & x(c) < x_{min}\\
	s(x), & x_{min} < x(c) <  x_{min} + \Delta\\
	0,     & x(c) > x_{min} + \Delta
	\end{cases}
	\label{eq:act_ineq}
\end{equation}

where $s(x)$ is any continuous function joining the two extrema, and $\Delta$ is a value that creates a buffer zone, where the inequality is already satisfied, but the activation value is still greater than zero, preventing chattering problems around the inequality control objective threshold $x_{min}$. An example of sigmoidal activation function is reported in Fig. \ref{fig:rrr_ineq}.
Throughout the thesis we will make use of the sigmoidal function so it's useful to define a compact notation to identify different types of sigmoids. Having $[x_{min}, x_{\textsc{max}}]$ the two minimum and maximum extrema for the sigmoid curve on the $x$ axis, and $[0,1]$ the two extrema for the sigmoid curve on the $y$ axis (the activation value), we can define:
\begin{itemize}
	\item An increasing sigmoidal activation function as
		\begin{equation}
			\sigmaIncr{ x_{min}}{x_{\textsc{max}}}
		\end{equation}
	\item A decreasing sigmoidal activation function as
		\begin{equation}
			\sigmaDecr{x_{min}}{x_{\textsc{max}}}
		\end{equation}
\end{itemize}
In general, for multidimensional control objective the activation function for a particular task $k$ will be a matrix of this type:
\begin{equation}\label{eq:actmatrix}
\qquad \vec{A}_{k} = \begin{bmatrix}
					a_{k,1} & 0      & 0   \\
					0   & \ddots & 0   \\
					0   & 0      & a_{k,l}
					\end{bmatrix},
\end{equation}%
%
where here $a_{k,1}$ is the $i$-th element on the trace of $\mat{A}$ for the $k$-th task.

For non-reactive control tasks, since the variable $x(\vec{c})$ is not defined, the activation function \eqref{eq:actfun} reduces to be $a(x) = a^i(x) \equiv 1$ as in Figure \ref{fig:rrr_eq}. From now on, the distinction between reactive and non-reactive control tasks will be dropped, and the generic term control task will be used, unless otherwise specified. 


\begin{figure}[t]
	\centering
	\subcaptionbox{\label{fig:rrr_eq}}{
		\def\svgwidth{0.44\textwidth}
		\input{images/theory/p_rrr_equality.eps_tex}
	}%
	\hfill
	\subcaptionbox{\label{fig:rrr_ineq}}{
		\def\svgwidth{0.44\textwidth}
		\input{images/theory/p_rrr_inequality.eps_tex}
	}%
	\caption{Comparison of task activations for equality and inequality control objectives. In figure \ref{fig:rrr_ineq} and example of $\sigmaDecr{x_{min}}{x_{min}+\Delta}$ decreasing sigmoidal function.}
	\label{fig:activation_function}
\end{figure}

\subsection{Action Transition}\label{sec:actiontran}
%\subsection{Task transitions}
The activation and deactivation of a task, apart from avoiding unnecessary system constraints, can serve another important purpose. In fact, it can be exploited for providing a smooth transition between actions, so to satisfy the requirements highlighted in \ref{sec:action}. 
Let us consider the following two actions $\mathcal{A}_1$ and $\mathcal{A}_2$, composed by objectives abstractly labelled with alphabetic letters, where $A \prec B$ denotes that $A$ has higher priority than $B$:
\begin{equation*}
\begin{aligned}
\mathcal{A}_1:\quad & A \prec B, C, D\\
\mathcal{A}_2:\quad & A \prec D \prec C, E\\
\end{aligned}
\end{equation*}
where $A, C, D$ are in common, but with $D$ at a different priority ordering w.r.t. $C$. Now consider the following merged list:
\begin{equation*}
\mathcal{U}:\quad A \prec D_1 \prec B, C, D_2, E;
\end{equation*}
where $D_1$ and $D_2$ represent the same objective D, but with a different priority.
It is clear that, through insertion/deletion of some of the entries, the two original lists can be reconstructed.
For example, $\mathcal{A}_1$ can be obtained by removing $D_1$ and E from the unified list. Conversely, $\mathcal{A}_2$ can be obtained by removing the
control objectives $D_2$ and $E$.

To make possible this transitions when switching between actions, the activation function \eqref{eq:actfun} is modified to become:
\begin{equation}\label{eq:actfun2}
a(x,\vec{p}) = a^i(x) a^p(\vec{p}),
\end{equation}
where $a^p(\vec{p}) \in [0, 1]$ is a continuous sigmoidal function of a vector of parameters $\vec{p}$ external to the control task itself. In particular, the $a^p(\vec{p})$ can be conveniently parametrized by the previous and current action being executed, and the time elapsed in the current action, to obtain the desired activation/deactivation smooth transition. Of course, this new activation function $a^p(\vec{p})$  applies to both to reactive and non-reactive control tasks since it does not depend on the variable $x(\vec{c})$. 
Having $N$ tasks, identified by their $k$ index, and two successive actions  $\mathcal{A}_1$ and  $\mathcal{A}_2$, we can define the following algorithm for smoothly transition between actions:

\begin{algorithm}
	\caption{Tasks transition algorithm}\label{alg:tasktransition}
	\begin{algorithmic}[1]
		%\State 
		\For{$k \gets 1 \textrm{ to } N$}
		\If {$\textrm{task}_k \in \mathcal{A}_1 \land \textrm{task}_k \in \mathcal{A}_2$}
		\State $a^p(\vec{p}) = 1$
		\EndIf
		\If {$\textrm{task}_k \in \mathcal{A}_1 \land \textrm{task}_k \notin \mathcal{A}_2$}
		\State $a^p(\vec{p}) = \textrm{decreasingSigmoid}(t)$ 
		\EndIf
		\If {$\textrm{task}_k \notin \mathcal{A}_1 \land \textrm{task}_k \in \mathcal{A}_2$}
		\State $a^p(\vec{p}) = \textrm{increasingSigmoid}(t)$
		\EndIf
		\EndFor		
	\end{algorithmic}
\end{algorithm}

Once all the actions that the system can execute have been defined, the unified list can be constructed. Many safety tasks will be common to all actions, and they will be at the same (high) priority level in each of them, therefore for such tasks it will result that $a^p(\vec{p}) \equiv 1$. Conversely, $a^p(\vec{p})$ will be mostly exploited to activate/deactivate action-defining objectives and prerequisite ones, calculated as in Algorithm \ref{alg:tasktransition}.  

\section{Task Priority Inverse Kinematics}\label{sec:tpik}

Given an action $\mathcal{A}$, for each priority level $k$, the following matrices and vectors are consequently defined:
\begin{itemize}
	\item $\dot{\bar{\vec{x}}}_k \triangleq \begin{bmatrix}\dot{\bar{x}}_{1,k} & \cdots & \dot{\bar{x}}_{m,k}\end{bmatrix}^T$ is the stacked vector of all the reference rates of the scalar control tasks;
	\item $\mat{J}_k$ is the Jacobian relationship  expressing the current rate-of-change of the $k$-th task vector $\begin{bmatrix}\dot{{x}}_{1,k} & \cdots & \dot{{x}}_{m,k}\end{bmatrix}^T$ w.r.t. the system velocity vector $\dot{\vec{y}}$, simply resulting from the stacking of the row vector ones in \eqref{eq:scalarjacrel};
	\item $\mat{A}_k \triangleq \textrm{diag}(a_{1,k},  \cdots,  a_{m,k})$ is the diagonal matrix of all the activation functions described in \eqref{eq:actfun2}.
\end{itemize}


Let us remark how inequalities introduced in section \ref{sec:objectives} are objectives to be eventually reached that, with the adoption of the reference law \eqref{eq:fbkrefrate}, are transformed in control tasks, i.e. velocity tracking minimization problems. However, as already mentioned, to avoid overconstraining the system, they must be deactivated whenever the inequality is satisfied.

To the aim of finding the system velocity reference vector $\dot{\bar{\vec{y}}}$ that satisfies the above expressed priority requirements between the different objectives, a straightforward idea is to employ the diagonal diagonal matrix $\mat{A}_k$ as a weight in the minimization process to deactivate tasks that are not anymore relevant, thus leading to the following sequence of nested minimization problems, corresponding to the so called Task Priority Inverse Kinematics (TPIK):
\begin{equation}\label{eq_original_problem}
S_k \triangleq \left\{ \arg \min_{\dot{\bar{\vec{y}}} \in S_{k-1}} \left\| \mat{A}_k (\dot{\bar{\vec{x}}}_k - \mat{J}_k \dot{\bar{\vec{y}}}) \right\|^2 \right\},\, k = 1, 2, \ldots, N,
\end{equation}
where $S_{k-1}$ is the manifold of solutions of all the previous tasks in the hierarchy, $S_0 \triangleq \mathbb{R}^n$, and $N$ is the total number of priority levels. Unfortunately, the task priority implementation as suggested above turns out to be affected by discontinuities in $\dot{\vec{y}}$ when $\mat{A}_k$ is varying some of its entries (i.e. within the so called transition zones) due to the fact that the resulting weighted pseudo-inverse $(\mat{A}_k\mat{J}_k)^\# \mat{A}_k$ is actually invariant to the weights on the rows of $\mat{J}_k$ that are linearly independent, as shown in \cite{Doty1993}.

In order to cope with these invariance problems, a specialized \emph{task oriented regularization} has been introduced in the minimization problem, as explained in \cite{Simetti2016d}. Such a specialized regularization, coupled with the classical singular value oriented one to deal with singularities of the involved matrices, was the novel idea of the proposed new pseudo inversion framework.

The adopted specialized regularization will be now briefly outlined, by now making reference to the following new form of TPIK problem itself   
\begin{equation}\label{eq_rminproblem}
S_k \triangleq \left\{ \arg \mathrm{R\textrm{-}}\min_{\dot{\bar{\vec{y}}} \in S_{k-1}} \left\| \mat{A}_k (\dot{\bar{\vec{x}}}_k - \mat{J}_k \dot{\bar{\vec{y}}}) \right\|^2 \right\},\, k = 1, 2, \ldots, N,
\end{equation}
where the notation $\mathrm{R\textrm{-}}\min$ is introduced for underlining that each minimization is performed via the specialized form of \cite{Simetti2016d}.

Within such a specialized framework (named \emph{iCAT: inequality Constraints And Task transitions}) the above TPIK problem \eqref{eq_rminproblem} results in the following algorithm, to be initialized with
\begin{equation}\label{eq_init}
\begin{aligned}
\vec{\rho}_{0} &= \vec{0}, & \mat{Q}_{0} &= \mat{I},
\end{aligned}
\end{equation}
and then, for $k = 1, \ldots, N$
\begin{equation}\label{eq_tri_centralized_alg}
\begin{aligned}
\mat{W}_k &=  \mat{J}_k \mat{Q}_{k-1} (\mat{J}_k \mat{Q}_{k-1})^{\#,\mat{A}_k,\mat{Q}_{k-1}}, \\
\mat{Q}_k &= \mat{Q}_{k-1} (\mat{I} - (\mat{J}_k \mat{Q}_{k-1})^{\#,\mat{A}_k,\mat{I}} {\mat{J}_k \mat{Q}_{k-1}}),\\
\vec{\rho}_k &= \vec{\rho}_{k-1} + \textrm{Sat}\left( \mat{Q}_{k-1} (\mat{J}_k \mat{Q}_{k-1})^{\#,\mat{A}_k,\mat{I}} \mat{W}_k \left(\dot{\bar{\vec{x}}}_k - \mat{J}_k \vec{\rho}_{k-1} \right) \right),
\end{aligned}
\end{equation}
where 
\begin{itemize}
	\item the special pseudo inverse operator $\mat{(\cdot)}^{\#,\mat{A},\mat{Q}}$ is strictly needed to cope the invariance problems mentioned above. Its definition is reported in \cite{Simetti2016d} and will be omitted here.
	\item $\vec{\rho}_k$ is the vector of the system control variables, which results constructed at the $k$-th minimization stage, through the last equation in \eqref{eq_tri_centralized_alg};
	\item $\left(\dot{\bar{\vec{x}}}_k - \mat{J}_k \vec{\rho}_{k-1} \right)$ is the $k$-th task reference rate minus the task space contribution of the $k-1$ control vector $\vec{\rho}_{k-1}$, i.e. the influence exerted on the current $k$-th task by the presence of all the higher priority tasks;
	\item $\mat{Q}_{k-1}$ is a $n \times n$ matrix that takes into account the control directions (totally or partially) used within all the higher priority tasks;
	\item $\mat{W}_k$ is a $m \times m$ matrix, where $m$ is the row-dimension of the control task at the current priority level, which is suitably structured as indicated to prevent discontinuities in the control variables occurring between different priority levels;
	\item The function Sat($\cdot$) implements the management of control variable saturations, accordingly with what suggested in \cite{Antonelli2009b}.
\end{itemize}
At the end of the above iterative process, the final system velocity vector is therefore $\dot{\bar{\vec{y}}} = \vec{\rho}_N.$ The interested reader can find all the relevant details of this procedure in \cite{Simetti2016d}, where a comparison with other task priority approaches, as for instance those appearing in \cite{Kanoun2011, Escande2014, Moe2016}, is given.