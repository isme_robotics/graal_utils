# LaTeX Thesis Template

Un template, fatto bene.

La classe utilizzata è **ortotesi.cls**, un template derivante dalla classe `memoir`.

Dentro il file di stile **ortopackages.sty** si possono trovare tutti i pacchetti utilizzati ed i nuovi comandi definiti.

Il documento è predisposto per generare un file *PDF/A Compliant* se necessario.
